package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    Task bindTaskToProjectById(final String userId, final String projectId, final String taskId);

    Task unbindTaskById(final String userId, final String id);

    void unbindAllTaskByProjectId(final String userId, final String id);

    Task findByName(final String userId, String name);

    Task removeByName(final String userId, String name);

    Task startById(final String userId, String id);

    Task startByName(final String userId, String name);

    Task startByIndex(final String userId, int index);

    Task finishById(final String userId, String id);

    Task finishByName(final String userId, String name);

    List <Task> findAllTaskByProjectId(String userId, final String id);

    Task finishByIndex(final String userId, int index);

    Task changeStatusById(final String userId, String id, Status status);

    Task changeStatusByName(final String userId, String name, Status status);

    Task changeStatusByIndex(final String userId, int index, Status status);

}