package ru.tsc.gavran.tm.repository;

import ru.tsc.gavran.tm.api.repository.IAuthRepository;

public class AuthRepository implements IAuthRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
