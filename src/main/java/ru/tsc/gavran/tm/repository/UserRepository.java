package ru.tsc.gavran.tm.repository;

import ru.tsc.gavran.tm.api.repository.IUserRepository;
import ru.tsc.gavran.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (User user : list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

}
