package ru.tsc.gavran.tm.repository;

import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.AbstractEntity;
import ru.tsc.gavran.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        list.sort(comparator);
        return list;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public E findById(final String id) {
        for (E entity : list)
            if (id.equals(entity.getId())) return entity;
        return null;
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return null;
    }

    @Override
    public E findByIndex(final Integer index) {
        if (list == null) return null;
        return list.get(index);
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public E removeByIndex(final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }
}
