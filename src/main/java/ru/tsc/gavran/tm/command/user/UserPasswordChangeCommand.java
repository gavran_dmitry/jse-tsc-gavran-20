package ru.tsc.gavran.tm.command.user;

import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change the user's password.";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth) throw new AccessDeniedException();
        System.out.println("ENTER USER ID: ");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new UserNotFoundException();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }
}