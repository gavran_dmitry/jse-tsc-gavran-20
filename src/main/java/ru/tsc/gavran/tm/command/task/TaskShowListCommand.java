package ru.tsc.gavran.tm.command.task;

import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.enumerated.Sort;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show a list of tasks.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(sortType.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
        System.out.println("[OK]");
    }

}