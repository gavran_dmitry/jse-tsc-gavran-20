package ru.tsc.gavran.tm.command.user;

import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin) throw new AccessDeniedException();
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        final String id = serviceLocator.getUserService().findByLogin(login).getId();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().removeByLogin(login);
    }

}