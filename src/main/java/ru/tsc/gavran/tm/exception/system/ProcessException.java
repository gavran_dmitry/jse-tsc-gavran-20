package ru.tsc.gavran.tm.exception.system;

import ru.tsc.gavran.tm.exception.AbstractException;

public class ProcessException extends AbstractException {

    public ProcessException() {
        super("Process error! Try again.");
    }

}
