package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty.");
    }

}
